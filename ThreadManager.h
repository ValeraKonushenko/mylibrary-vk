#pragma once
#include "namespaces_decl.h"
#include <mutex>
#include <vector>
#include <thread>
VK_NAMESPACE__VK_SAFE__BEG
class ThreadManager {
public:
	enum class Mode {
		join,
		detach
	};
	typedef std::thread thread;
private:
	void __init__()noexcept;
protected:
	mutable std::recursive_mutex rec_mut;
	Mode mode;
	std::vector<thread*> data;
public:
	size_t push(thread *new_thread)noexcept(false);
	void setMode(Mode mode)noexcept;
	Mode getMode()const noexcept;
	ThreadManager(Mode mode = Mode::join)noexcept;
	~ThreadManager();
	ThreadManager(const ThreadManager&) = delete;
	ThreadManager(ThreadManager&& obj)noexcept;
	ThreadManager& operator=(ThreadManager&) = delete;
	ThreadManager& operator=(ThreadManager&& obj) noexcept;
};
VK_NAMESPACE__VK_SAFE__END