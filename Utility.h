#pragma once
#define VK_CHECK_FOR_1B_CHAR(type) static_assert(sizeof(type) == 1, \
								   "This functions can't work so next methods"\
								   "working only with 1byte char. Change method"\
								   "for working with 2byte characters");
