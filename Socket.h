#pragma once
#include "_WSAData.h"
#include "Logger.h"
#include "SocketConnection.h"
#include "Buffer.h"
#include "namespaces_decl.h"
#include <mutex>






VK_NAMESPACE__VK_UDP_SAFE__BEG
class Socket {
public:
	typedef int port;
	typedef vk::safe::Buffer Buffer;
private:
protected:
	mutable std::recursive_mutex	recmut;

	static WSADATA*			wsa;
	vk::safe::SocketConnection	sc;
	void				createSocket	(port p)					noexcept(false);
public:
	void				setNonBlock		(u_long is_block)				noexcept(false);
	void				close			()							noexcept;
	bool				isOpen			()							const noexcept;
	vk::safe::SocketConnection	getConnection	()					const noexcept;

	Socket()														noexcept(false);
	virtual ~Socket()												= default;
	Socket(const Socket&)											noexcept;
	Socket& operator=(const Socket&)								noexcept;
	Socket(Socket&&)												noexcept;
	Socket& operator=(Socket&&)										noexcept;
};
VK_NAMESPACE__VK_UDP_SAFE__END














VK_NAMESPACE__VK_TCP_SAFE__BEG
class Socket {
public:
	typedef int port;
	typedef vk::safe::Buffer Buffer;
private:
protected:
	mutable std::recursive_mutex	recmut;

	static WSADATA*			wsa;
	vk::safe::SocketConnection	sc;
	bool				createSocket	(port p)					noexcept;
	virtual void		printStep(const char*, bool smth = false)	const noexcept;
public:
	void send(SOCKET out, const Buffer& bf, size_t size, int flags = 0)const  noexcept;
	bool reciev(SOCKET client, Buffer& buff, int size, int flags = 0)const noexcept;
	bool				setBlock		(bool is_block)				noexcept;
	void				close			()							noexcept;
	bool				isOpen			()							const noexcept;
	vk::safe::SocketConnection	getConnection	()					const noexcept;
	Socket();
	operator SOCKET()												const noexcept;
	operator vk::safe::SocketConnection()							const noexcept;
	virtual ~Socket()												= default;
	Socket(const Socket&)											noexcept;
	Socket& operator=(const Socket&)								noexcept;
	Socket(Socket&&)												noexcept;
	Socket& operator=(Socket&&)										noexcept;
};
VK_NAMESPACE__VK_TCP_SAFE__END

VK_NAMESPACE__VK_TCP_UNSAFE__BEG
	class Socket {
	public:
		typedef int port;
		typedef vk::unsafe::Buffer Buffer;
	private:
	protected:
		static WSADATA*			wsa;
		vk::unsafe::SocketConnection	sc;
			
		bool				createSocket	(port p, int sock_type = SOCK_STREAM)					noexcept;
		virtual void		printStep(const char*, bool smth = false)	const noexcept;
	public:
		void send(SOCKET out, const Buffer& bf, size_t size, int flags = 0)const  noexcept;
		void reciev(SOCKET client, Buffer& buff, int size, int flags = 0)const noexcept;
		Buffer reciev(SOCKET client, int size, int flags = 0)	const noexcept;

		void				close			()							noexcept;
		bool				isOpen			()							const noexcept;
		vk::unsafe::SocketConnection	getConnection	()				const noexcept;
		Socket();
		operator SOCKET()												const noexcept;
		operator vk::unsafe::SocketConnection()							const noexcept;
		virtual ~Socket()												= default;
		Socket(const Socket&)											noexcept;
		Socket& operator=(const Socket&)								noexcept;
		Socket(Socket&&)												noexcept;
		Socket& operator=(Socket&&)										noexcept;
	};
VK_NAMESPACE__VK_TCP_UNSAFE__END