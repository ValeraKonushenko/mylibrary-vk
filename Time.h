#pragma once
#include "namespaces_decl.h"
#include <string>
#include <time.h>

VK_NAMESPACE__VK__BEG
class Time {
private:
	Time()							= delete;
	~Time()							= delete;
	Time& operator=(const Time&)	= delete;
	Time& operator=(Time&&)			= delete;
	Time(const Time&)				= delete;
	Time(Time&&)					= delete;
public:
	static std::string getLocalTime(time_t unixtime);
	static std::string getLocalTime();
};

VK_NAMESPACE__VK__END
