#pragma once
#include "namespaces_decl.h"
#include "IDCounter.h"
#include "Container.h"
#include <mutex>
VK_NAMESPACE__VK_UDP_SAFE__BEG
/*
* This class store data for UDP connection:
* Structure:
*		{| ID's 8-byte | Flag's 1-byte | data' N-byte |}
* Full size: ID's size + data' size;
*/
class Package {
public:
	typedef char		byte;	
	typedef int			size_t;
	enum Flag : char {
		binary = 1,
		header,
		body,
		body_bmp,
		answer
	};
	class Predicate {
	public:
		bool operator()(const Package& a, const Package& b);
	};

private:
	mutable std::recursive_mutex		recmut;
	vk::safe::IDCounter::id_t	id;
	byte						flag;
	byte*						storage;
	size_t						size;

	void			__init			();
protected:

public:
	vk::safe::IDCounter::id_t			getId	()const noexcept(false);
	vk::unsafe::Container<byte>			getData	()const noexcept(false);
	static byte							getFlag	(const Package&p)noexcept(false);
					Package			(size_t sz = 0)		noexcept(false);
	bool			isEmpty			()					const noexcept(false);
	void			fill			(byte el)			noexcept(false);
	void			setBufferSize	(size_t sz)			noexcept(false);
	size_t			getBufferSize	()					const noexcept(false);
	size_t			getHeaderSize	()					const noexcept;
	size_t			getFullSize		()					const noexcept;
	void			clear			()					noexcept;
	byte			get				(size_t i)			const noexcept(false);
	void			set				(size_t i, byte el)	noexcept(false);
	void			set				(size_t size, const void* el)noexcept(false);
	void			setID			(vk::safe::IDCounter::id_t id)noexcept;
	void			setFlag			(byte flag)			noexcept;
	byte 			getFlag			()					noexcept;
	vk::unsafe::Container<byte> inspect	()					const noexcept(false);
	~Package();
	Package(const Package&obj)				noexcept;
	Package(Package&&obj)					noexcept;
	Package& operator=(const Package& obj)	noexcept;
	Package& operator=(Package&& obj)		noexcept;
};
VK_NAMESPACE__VK_UDP_SAFE__END