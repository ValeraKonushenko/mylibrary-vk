#include "Address.h"
#include <stdexcept>
#include <string>
VK_NAMESPACE__VK_UDP_SAFE__BEG
std::string Address::getFullAddress() const noexcept(false){
	std::lock_guard lg(recmut);
	std::string out;
	for (size_t i = 0; i < 4; i++) {
		out += std::to_string(n[i]);
		out += ".";
	}
	out += ":";
	out += std::to_string(port);
	return std::move(out);
}
std::wstring Address::getFullAddressW() const noexcept(false){
	std::lock_guard lg(recmut);
	std::wstring out;
	for (size_t i = 0; i < 4; i++) {
		out += std::to_wstring(n[i]);
		out += L".";
	}
	out += L":";
	out += std::to_wstring(port);
	return std::move(out);
}
void Address::set(int port, const wchar_t*p) noexcept(false){
	std::lock_guard lg(recmut);
	unsigned int n[4]{};
	n[0] = _wtoi(p);
	p = wcschr(p, L'.') + 1;
	n[1] = _wtoi(p);
	p = wcschr(p, L'.') + 1;
	n[2] = _wtoi(p);
	p = wcschr(p, L'.') + 1;
	n[3] = _wtoi(p);
	set(port, n[0], n[1], n[2], n[3]);
}
void Address::set(int port, const char* p) noexcept(false) {
	std::lock_guard lg(recmut);
	unsigned int n[4]{};
	n[0] = atoi(p);
	p = strchr(p, L'.') + 1;
	n[1] = atoi(p);
	p = strchr(p, L'.') + 1;
	n[2] = atoi(p);
	p = strchr(p, L'.') + 1;
	n[3] = atoi(p);
	set(port, n[0], n[1], n[2], n[3]);
}
Address::node Address::getNode(char i) const noexcept(false){
	std::lock_guard lg(recmut);
	if (i >= 4)
		throw std::runtime_error("The node's index can't be greater then 3");
	if (i < 0)
		throw std::runtime_error("The node's index can't be less then 0");
	return n[i];
}
int Address::getPort() const noexcept{
	std::lock_guard lg(recmut);
	return port;
}
void Address::set(int port, node a, node b, node c, node d) noexcept(false){
	std::lock_guard lg(recmut);
	if(a > 255 || a < 0 || 
	   b > 255 || b < 0 || 
	   c > 255 || c < 0 || 
	   d > 255 || d < 0
	   )
		throw std::runtime_error("The node can't be greater then 255 or less then 0");
	n[0] = a;
	n[1] = b;
	n[2] = c;
	n[3] = d;
	this->port = port;
}
void Address::set(int port, node a) noexcept(false){
	std::lock_guard lg(recmut);
	int nodes[4]{};
	nodes[0] = (a << 0 ) >> 24;
	nodes[1] = (a << 8 ) >> 24;
	nodes[2] = (a << 16) >> 24;
	nodes[3] = (a << 24) >> 24;
	set(port, nodes[0], nodes[1], nodes[2], nodes[3]);
}
void Address::setPort(int port) noexcept{
	std::lock_guard lg(recmut);
	this->port = port;
}
void Address::clear(){
	std::lock_guard lg(recmut);
	memset(n,0,sizeof(n));
	port = 0;
}
Address::Address(int port, node a, node b, node c, node d) noexcept(false){
	clear();
	set(port,a,b,c,d);
}
bool Address::operator==(const Address& adr) const noexcept{
	std::lock_guard lg(recmut);
	return 
		n[0] == adr.n[0] &&
		n[1] == adr.n[1] &&
		n[2] == adr.n[2] &&
		n[3] == adr.n[3] &&
		port == adr.port;
}
bool Address::operator!=(const Address& adr) const noexcept{
	std::lock_guard lg(recmut);
	return !(*this == adr);
}
unsigned int Address::getDestAddr() const noexcept{
	std::lock_guard lg(recmut);
	return (n[0] << 24) | (n[1] << 16) | (n[2] << 8) | n[3];
}
Address::Address(const Address&obj) noexcept{
	std::scoped_lock sl(recmut, obj.recmut);
	*this = obj;
}
Address::Address(Address&&obj) noexcept{
	std::scoped_lock sl(recmut, obj.recmut);
	*this = std::move(obj);
}
Address& Address::operator=(const Address&obj) noexcept{
	std::scoped_lock sl(recmut, obj.recmut);
	memcpy(n, obj.n, sizeof(n));
	this->port = obj.port;
	return *this;
}
Address& Address::operator=(Address&&obj) noexcept{
	std::scoped_lock sl(recmut, obj.recmut);
	memcpy(n, obj.n, sizeof(n));
	this->port = obj.port;

	//------zeroing------
	obj.port = static_cast<decltype(obj.port)>(0);
	memset(obj.n, 0, sizeof(obj.n));
	return *this;
}
VK_NAMESPACE__VK_UDP_SAFE__END
