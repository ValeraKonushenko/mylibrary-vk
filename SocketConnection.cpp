//#include "pch.h"
#include "SocketConnection.h"
#pragma warning(disable: 4172)
namespace vk::safe {
void				SocketConnection::clear				() {
	std::lock_guard lg(recmut);
	this->_port		= -1;
	this->_socket	= INVALID_SOCKET;
	memset(&this->sin, 0, sizeof(this->sin));
}
SOCKET				SocketConnection::socket			()const {
	std::lock_guard lg(recmut);
	return _socket;
}
void				SocketConnection::socket			(SOCKET s){
	std::lock_guard lg(recmut);
	if (s < 0)	throw std::exception("The socket can't be smaller than 0");
	this->_socket = s;
}
const SOCKADDR_IN&	SocketConnection::sockaddr			()const {
	std::lock_guard lg(recmut);
	SOCKADDR_IN tmp = sin;
	return tmp;
}
void				SocketConnection::sockaddr			(const SOCKADDR_IN &s) {
	std::lock_guard lg(recmut);
	this->sin = s;
}
u_long				SocketConnection::port				()const {
	std::lock_guard lg(recmut);
	return _port;
}
void				SocketConnection::port				(u_long p) {
	std::lock_guard lg(recmut);
	if (p < 0)	throw std::exception("The port can't be smaller than 0");
	this->_port = p;
}
bool				SocketConnection::isInvalidSocket	()const {
	std::lock_guard lg(recmut);
	return this->_socket == INVALID_SOCKET;
}
SocketConnection::operator SOCKET						()const noexcept{
	std::lock_guard lg(recmut);
	return this->_socket;
}
//SocketConnection::operator SOCKADDR_IN() const noexcept{
//	std::lock_guard lg(recmut);
//	return this->sin;
//}
SocketConnection&	SocketConnection::operator=			(const SocketConnection& sc)noexcept {
	std::scoped_lock sl(this->recmut, sc.recmut);
	port(sc._port);
	socket(sc._socket);
	this->sockaddr(sc.sin);
	return *this;
}
SocketConnection::SocketConnection						(SocketConnection&& sc)noexcept {
	std::scoped_lock sl(this->recmut, sc.recmut);
	*this = std::move(sc);
}
SocketConnection& SocketConnection::operator=			(SocketConnection&& sc)noexcept {
	std::scoped_lock sl(this->recmut, sc.recmut);
	this->sockaddr(sc.sin);
	this->socket(sc._socket);
	this->port(sc._port);
	return *this;
}
SocketConnection::SocketConnection						() noexcept {
	clear();
}
SocketConnection::SocketConnection						(const SocketConnection& sc)noexcept {
	std::scoped_lock sl(this->recmut, sc.recmut);
	*this = sc;
}
}




namespace vk::unsafe {
void				SocketConnection::clear				() {
	this->_port		= -1;
	this->_socket	= INVALID_SOCKET;
	memset(&this->sin, 0, sizeof(this->sin));
}
SOCKET				SocketConnection::socket			()const {
	return _socket;
}
void				SocketConnection::socket			(SOCKET s){
	if (s < 0)	throw std::exception("The socket can't be smaller than 0");
	this->_socket = s;
}
const SOCKADDR_IN&	SocketConnection::sockaddr			()const {
	SOCKADDR_IN tmp = sin;
	return tmp;
}
void				SocketConnection::sockaddr			(const SOCKADDR_IN &s) {
	this->sin = s;
}
u_long				SocketConnection::port				()const {
	return _port;
}
void				SocketConnection::port				(u_long p) {
	if (p < 0)	throw std::exception("The port can't be smaller than 0");
	this->_port = p;
}
bool				SocketConnection::isInvalidSocket	()const {
	return this->_socket == INVALID_SOCKET;
}
SocketConnection::operator SOCKET						()const noexcept{
	return this->_socket;
}
SocketConnection&	SocketConnection::operator=			(const SocketConnection& sc)noexcept {
	port(sc._port);
	socket(sc._socket);
	this->sockaddr(sc.sin);
	return *this;
}
SocketConnection::SocketConnection						(SocketConnection&& sc)noexcept {
	*this = std::move(sc);
}
SocketConnection& SocketConnection::operator=			(SocketConnection&& sc)noexcept {
	this->sockaddr(sc.sin);
	this->socket(sc._socket);
	this->port(sc._port);
	return *this;
}
SocketConnection::SocketConnection						() noexcept {
	clear();
}
SocketConnection::SocketConnection						(const SocketConnection& sc)noexcept {
	*this = sc;
}
//SocketConnection::operator SOCKADDR_IN() const noexcept {
//	return this->sin;
//}
}
#pragma warning(default: 4172)