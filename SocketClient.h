#pragma once
#include "Socket.h"


VK_NAMESPACE__VK_TCP_SAFE__BEG
class SocketClient : public vk::tcp::safe::Socket{
	private:
	protected:
		bool	createSin(PCWSTR addr);
	public:
		bool	open(port p, PCWSTR addr)				noexcept;
				SocketClient()							= default;
		void	send(const Buffer& bf, size_t size, int flags = 0)	const  noexcept;
		bool	connect()								noexcept;
		std::string getInetNtop()						const noexcept;
		virtual ~SocketClient()							= default;
		SocketClient(const SocketClient&)			;
		SocketClient& operator=(const SocketClient&);
		SocketClient(SocketClient&&)					noexcept;
		SocketClient& operator=(SocketClient&&)			noexcept;

	};
VK_NAMESPACE__VK_TCP_SAFE__END


VK_NAMESPACE__VK_TCP_UNSAFE__BEG
class SocketClient : public vk::tcp::unsafe::Socket {
private:
protected:
	bool	createSin(PCWSTR addr);
public:
	bool	open(port p, PCWSTR addr)				noexcept;
	SocketClient() = default;
	void	send(const Buffer& bf, size_t size, int flags = 0)	const  noexcept;
	bool	connect()								noexcept;
	std::string getInetNtop()						const noexcept;
	virtual ~SocketClient() = default;
	SocketClient(const SocketClient&);
	SocketClient& operator=(const SocketClient&);
	SocketClient(SocketClient&&)					noexcept;
	SocketClient& operator=(SocketClient&&)			noexcept;

};
VK_NAMESPACE__VK_TCP_UNSAFE__END
