#include "CodeConvert.h"
#include <stringapiset.h>
#include <string.h>
std::string vk::utf8_encode(const std::wstring& src){
    #ifdef _WIN32

    if (src.empty()) return std::string();
    int size_needed = WideCharToMultiByte(CP_UTF8, 0, &src[0], static_cast<int>(src.size()), nullptr, 0, nullptr, nullptr);
    std::string strTo(size_needed, 0);
    WideCharToMultiByte(CP_UTF8, 0, &src[0], static_cast<int>(src.size()), &strTo[0], size_needed, nullptr, nullptr);
    return strTo;

    #else
    #error "The platform not supported"
    #endif
}
