#include "ThreadManager.h"
VK_NAMESPACE__VK_SAFE__BEG

void ThreadManager::__init__() noexcept{
	setMode(Mode::join);
	data.clear();
}
size_t ThreadManager::push(thread* new_thread) noexcept(false){
	std::lock_guard lg(rec_mut);
	if (!new_thread)
		throw std::exception("The pointer to thread is nullptr");
	data.push_back(new_thread);
	return data.size();
}
void ThreadManager::setMode(Mode mode) noexcept{
	std::lock_guard lg(rec_mut);
	this->mode = mode;
}
ThreadManager::Mode ThreadManager::getMode() const noexcept{
	std::lock_guard lg(rec_mut);
	return mode;
}
ThreadManager::ThreadManager(Mode mode) noexcept{
	__init__();
	setMode(mode);
}
ThreadManager::~ThreadManager(){
	std::lock_guard lg(rec_mut);
	for (int i = static_cast<int>(data.size()) - 1; i >= 0; i--) {
		if (mode == Mode::join)
			data[i]->join();
		else if(mode == Mode::detach)
			data[i]->detach();
		delete data[i];
	}
}
ThreadManager::ThreadManager(ThreadManager&& obj) noexcept{
	std::scoped_lock sl(rec_mut, obj.rec_mut);
	*this = std::move(obj);
}
ThreadManager& ThreadManager::operator=(ThreadManager&& obj) noexcept{
	std::scoped_lock sl(rec_mut, obj.rec_mut);
	data = std::move(obj.data);
	mode = obj.mode;
	obj.__init__();
	return *this;
}

VK_NAMESPACE__VK_SAFE__END