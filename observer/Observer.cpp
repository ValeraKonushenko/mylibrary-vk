//#include "pch.h"
#include "Observer.h"
namespace vk::safe {
	void Observer::subscribe		(Subscriber& sbr)noexcept(false) {
		std::lock_guard lg(mut);		
		if (!sbr.isFunc())
			throw std::exception("No active function in the subscriber");

		vec.push_back(std::move(sbr));
	}
	void Observer::unsubscribe		(const Subscriber& sbr)noexcept {
		std::lock_guard lg(mut);
		
		for (size_t i = 0u; i < vec.size(); i++) {
			if (vec[i] == sbr) {
				vec.erase(vec.begin() + i);
			}
		}
	}
	void Observer::notify			(Subscriber::event_id id) const noexcept {
		std::lock_guard lg(mut);
		auto i   = vec.begin();
		auto end = vec.end();
		for (;i != end; i++) {
			if (id == (*i).getEventId()) {
				(*i).UDPate();
			}
		}
	}
	Observer::Observer				(const Observer& sb) noexcept	{
		std::scoped_lock sl(mut, sb.mut);
		*this = sb;
	}
	Observer::Observer				(Observer&& sb) noexcept	{
		std::scoped_lock sl(mut, sb.mut);
		*this = std::move(sb);
	}
	Observer& Observer::operator=	(const Observer& sb) noexcept{
		std::scoped_lock sl(mut, sb.mut);
		//vec = sb.vec;
		throw "NO IMPLEMENT!!!!!!!!!";
		return *this;
	}
	Observer& Observer::operator=	(Observer&& sb) noexcept{
		std::scoped_lock sl(mut, sb.mut);
		//vec = std::move(sb.vec);
		throw "NO IMPLEMENT!!!!!!!!!";
		return *this;
	}
}




namespace vk::unsafe {
	void Observer::subscribe		(Subscriber& sbr)noexcept {
		vec.push_back(std::move(sbr));
	}
	void Observer::unsubscribe		(const Subscriber& sbr)noexcept {
		auto i = vec.begin();
		for (; i != vec.end(); i++) {
			if (*i == sbr) {
				vec.erase(i);
			}
		}
	}
	void Observer::notify			(Subscriber::event_id id) const noexcept {
		auto i   = vec.begin();
		auto end = vec.end();
		for (; i != end; i++) {
			if (id == (*i).getEventId()) {
				(*i).UDPate();
			}
		}
	}
	//Observer::Observer				(const Observer& sb) noexcept {
	//	*this = sb;
	//}
	Observer::Observer				(Observer&& sb) noexcept {
		*this = std::move(sb);
	}
	//Observer& Observer::operator=	(const Observer& sb) noexcept {
	//	vec = sb.vec;
	//	return *this;
	//}
	Observer& Observer::operator=	(Observer&& sb) noexcept {
		vec = std::move(sb.vec);
		return *this;
	}
}