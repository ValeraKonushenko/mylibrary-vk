//#include "pch.h"
#include "Subscriber.h"

namespace vk::safe {
	Subscriber::hash_t Subscriber::hash_counter = 0;

	void Subscriber::set(event_id id, func_t event_func) noexcept{
		std::lock_guard lg(recmut);
		this->id   = id;
		this->func = event_func;
	}

	bool Subscriber::isFunc() const noexcept{
		std::lock_guard lg(recmut);
		return static_cast<bool>(func);
	}

	Subscriber::Subscriber() noexcept :
		hash(hash_counter.load()),
		id(0){
		hash_counter++;
	}

	Subscriber::Subscriber(Subscriber::event_id id, Subscriber::func_t event_func)
		noexcept :
		hash(hash_counter.load()),
		func(event_func),
		id(id)  {
		hash_counter++;
	}
	Subscriber::hash_t Subscriber::getHash()const noexcept {
		return hash.load();
	}
	Subscriber::event_id Subscriber::getEventId()const noexcept {
		std::lock_guard lg(recmut);
		return id;
	}
	void Subscriber::UDPate()const noexcept {
		if(func)func();
	}
	bool Subscriber::operator==(const Subscriber& sb)const noexcept {
		std::lock_guard lg(recmut);
		return id == sb.id && hash == sb.hash;
	}
	bool Subscriber::operator!=(const Subscriber& sb)const noexcept {
		std::lock_guard lg(recmut);
		return id != sb.id && hash != sb.hash;
	}
	Subscriber::Subscriber(Subscriber&& sb) noexcept{
		*this = std::move(sb);
	}
	Subscriber& Subscriber::operator=(Subscriber&& sb) noexcept	{		
		std::scoped_lock lg(recmut, sb.recmut);
		this->func = sb.func;
		this->hash.store(sb.hash.load());
		this->id = sb.id;
		sb.func = nullptr;
		sb.hash.store(0ul);
		sb.id	= 0u;
		return *this;
	}
}


namespace vk::unsafe {
	Subscriber::hash_t Subscriber::hash_counter = 0;

	Subscriber::Subscriber(Subscriber::event_id id, Subscriber::func_t event_func) noexcept :
		hash(hash_counter),
		func(event_func),
		id(id)  {
		hash_counter++;
	}
	Subscriber::hash_t Subscriber::getHash()const noexcept {
		return hash;
	}
	Subscriber::event_id Subscriber::getEventId()const noexcept {
		return id;
	}
	void Subscriber::UDPate()const noexcept {
		func();
	}
	bool Subscriber::operator==(const Subscriber& sb)const noexcept {
		return id == sb.id && hash == sb.hash;
	}
	bool Subscriber::operator!=(const Subscriber& sb)const noexcept {
		return id != sb.id && hash != sb.hash;
	}
	Subscriber::Subscriber(Subscriber&& sb) noexcept :
		hash(sb.hash),
		func(sb.func),
		id(sb.id) {
		memset(&sb, 0, sizeof(sb));
	}
	Subscriber& Subscriber::operator=(Subscriber&& sb) noexcept {
		this->func = sb.func;
		this->hash = sb.hash;
		this->id = sb.id;
		sb.func = nullptr;
		sb.hash = 0ul;
		sb.id = 0u;
		return *this;
	}
}