#pragma once
#include "namespaces_decl.h"
#include <WinSock2.h>
#include <mutex>

VK_NAMESPACE__VK_SAFE__BEG
class SocketConnection {
private:
protected:
	mutable std::recursive_mutex	recmut;

	SOCKADDR_IN			sin;
	SOCKET				_socket;
	u_long				_port;
public:
	void				clear				();
	SOCKET				socket				()						const;
	void				socket				(SOCKET s)noexcept(false);
	const SOCKADDR_IN&	sockaddr			()						const;
	void				sockaddr			(const SOCKADDR_IN &s);
	u_long				port				()						const;
	void				port				(u_long p);
	bool				isInvalidSocket		()						const;
						operator SOCKET		()						const noexcept;
//						operator SOCKADDR_IN()						const noexcept;

	SocketConnection()												noexcept;
	SocketConnection(const SocketConnection&sc)						noexcept;
	virtual ~SocketConnection()										= default;
	SocketConnection& operator=(const SocketConnection&sc)			noexcept;
	SocketConnection(SocketConnection&&sc)							noexcept;
	SocketConnection& operator=(SocketConnection&&sc)				noexcept;
};
VK_NAMESPACE__VK_SAFE__END


VK_NAMESPACE__VK_UNSAFE__BEG
class SocketConnection {
private:
protected:
	SOCKADDR_IN					sin;
	SOCKET						_socket;
	u_long						_port;
public:
	void				clear				();
	SOCKET				socket				()						const;
	void				socket				(SOCKET s);
	const SOCKADDR_IN&	sockaddr			()						const;
	void				sockaddr			(const SOCKADDR_IN &s);
	u_long				port				()						const;
	void				port				(u_long p);
	bool				isInvalidSocket		()						const;
						operator SOCKET		()						const noexcept;
//						operator SOCKADDR_IN()						const noexcept;

	SocketConnection()												noexcept;
	SocketConnection(const SocketConnection&sc)						noexcept;
	virtual ~SocketConnection()										= default;
	SocketConnection& operator=(const SocketConnection&sc)			noexcept;
	SocketConnection(SocketConnection&&sc)							noexcept;
	SocketConnection& operator=(SocketConnection&&sc)				noexcept;
};

VK_NAMESPACE__VK_UNSAFE__END