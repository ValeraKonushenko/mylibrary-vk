#pragma once
#include "Socket.h"
#include "Address.h"
#include "Package.h"
#include "Pack.h"


VK_NAMESPACE__VK_UDP_SAFE__BEG
class UDPSocket : public vk::UDP::safe::Socket {
private:
	int getDataFromClient(Address& adr_answ, size_t size, int wait = 5000)noexcept(false);
protected:
	//vk::safe::IDCounter counter;
	void _bind()									noexcept(false);
	bool is_rec_data;
public:
	enum AnswerFlags {
		data_was_got,
		not_full_pack_of_package
	};
	void open(port p)								noexcept(false);
	bool isRecData()								const noexcept;
/*
* We can send some data using few version of overload functions.
* -U can send any data(like bytes) - is the first case
* 
* -Second case - using Package. Package is a wrapper of custom sending of data.
*	Using package u can send specified size of any data and get it like package.
*	The packages can't define whether it was delivered or not. U just send the
*	package and it can come or, may be, no. 
*	Also, u can't send data any size, so a package has a limit data' size
*	U should know about it: when u use Package, and size for it, this doesn't 
*	take into accout the size of header's part. The header's part also has a size,
*	is ID+flag. Now it take sizes of: unigned long long + char
*	
* -Third cast - u can use a Pack. Pack is set of packages. U can transfer data
*	of any value using it, so a Pack break to piaces for sending like one package
*	Advantages:
*	- to control of recieving of data
*	- sending any size data, this splits the data by packages on its own
*	- getting of data happens in the right order
*	- reported the sender about data delivery(delivered\lost)
*/
	bool sendto(Address adr, const char* data, int size)const noexcept(false);
	bool sendto(Address adr, const Package& p) const noexcept(false);
	bool sendto(Address adr, Pack& p, int flag = Package::Flag::body) noexcept(false);
	int  recvfrom(Address& from, char *buff, size_t buff_sz)const noexcept(false);
	int  recvfrom(Address& from, Package&)const noexcept(false);
	UDPSocket();
	virtual ~UDPSocket()					= default;
	UDPSocket(const UDPSocket&);
	UDPSocket& operator=(const UDPSocket&);
	UDPSocket(UDPSocket&&)					noexcept;
	UDPSocket& operator=(UDPSocket&&)		noexcept;

};
VK_NAMESPACE__VK_UDP_SAFE__END








VK_NAMESPACE__VK_TCP_SAFE__BEG
class SocketServer : public vk::tcp::safe::Socket{
private:
protected:
	bool _bind()									noexcept;
	bool _listen()									noexcept;
public:
	bool	open(port p)							noexcept;
	SocketServer()									= default;
	virtual ~SocketServer()							= default;
	SocketServer(const SocketServer&)			;
	SocketServer& operator=(const SocketServer&);
	SocketServer(SocketServer&&)					noexcept;
	SocketServer& operator=(SocketServer&&)			noexcept;

};
VK_NAMESPACE__VK_TCP_SAFE__END


VK_NAMESPACE__VK_TCP_UNSAFE__BEG
class SocketServer : public vk::tcp::unsafe::Socket {
private:
protected:
	bool _bind()									noexcept;
	bool _listen()									noexcept;
public:
	bool	open(port p)							noexcept;
	SocketServer()									= default;
	virtual ~SocketServer()							= default;
	SocketServer(const SocketServer&);
	SocketServer& operator=(const SocketServer&);
	SocketServer(SocketServer&&)					noexcept;
	SocketServer& operator=(SocketServer&&)			noexcept;

};
VK_NAMESPACE__VK_TCP_UNSAFE__END