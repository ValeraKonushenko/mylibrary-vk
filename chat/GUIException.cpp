#include "GUIException.h"
VK_NAMESPACE__VK__BEG
GUIException::GUIException(const std::string& msg) : 
	std::runtime_error(msg){
}
char const* GUIException::what() const{
	return std::runtime_error::what();
}

VK_NAMESPACE__VK__END