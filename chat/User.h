#pragma once
#include <mutex>
#include <string>

namespace vk::chat::safe {
class User {
public:
	typedef std::string string;
	typedef unsigned long long ID_t;

	static const ID_t	invalid_id;
	static const string invalid_name;

private:
protected:
	string	_name;
	ID_t	_id;
	mutable std::recursive_mutex recmut;
	void __init();
public:
	string		name	()				const noexcept;
	bool		name	(string str)	noexcept;
	ID_t		id		()				const noexcept;
	bool		id		(ID_t nid)		noexcept;


	User();
	~User();
	User(const User& obj)				noexcept;
	User(User&& obj)					noexcept;
	User& operator=(const User& obj)	noexcept;
	User& operator=(User&& obj)			noexcept;
};
}