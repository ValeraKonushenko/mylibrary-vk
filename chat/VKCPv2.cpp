#include "VKCPv2.h"
#include "VKCPv2.h"
#include "../Assert.h"
VK_NAMESPACE__VK_PROTO__BEG

const unsigned int	VKCPv2::size_of_header = sizeof(VKCPv2::Header);
const float			VKCPv2::proto_version = 2.0;


VKCPv2::data_t VKCPv2::generate(float v, MessageType type, unsigned int bd_sz, 
								const char* useragent, Status status)noexcept(false) {
	data_t data;
	data.reserve(size_of_header);
	data.set(0);
	char* p = data.data();
	
	//adding the version
	*reinterpret_cast<float*>(p) = v;
	p += sizeof(Header::version);

	//adding the message type
	*reinterpret_cast<MessageType*>(p) = type;
	p += sizeof(Header::type);
	
	//adding the body's size
	*reinterpret_cast<unsigned int*>(p) = bd_sz;
	p += sizeof(Header::body_size);
	
	//adding the useragent
	int length = sizeof(Header::useragent) / sizeof(char);
	if (strlen(useragent) >= length)
		throw vk::exception("Too big length of the useragent",__FILE__,__FUNCTION__,__LINE__);
	for (int i = 0; i < length && useragent[i]; i++)
		p[i] = useragent[i];
	p += sizeof(Header::useragent);
	
	//adding the satus
	*reinterpret_cast<Status*>(p) = status;
	p += sizeof(Status);

	//p -= sizeof(Header);
	//for (int i = 0; i < 80; i++) {
	//	if (p[i] == 0)
	//		p[i] = 32;
	//}

	return std::move(data);
}

VKCPv2::Header VKCPv2::parse(vk::unsafe::Container_c data) noexcept(false){
	Header header{};
	char* p = data;
	header.version = *reinterpret_cast<decltype(header.version)*>(p);
	p += sizeof(decltype(header.version));
	header.type = *reinterpret_cast<decltype(header.type)*>(p);
	p += sizeof(decltype(header.type));
	header.body_size = *reinterpret_cast<decltype(header.body_size)*>(p);
	p += sizeof(decltype(header.body_size));
	
	int i = 0;
	int length = strlen(p);
	if(length >= sizeof(Header::useragent) / sizeof(char))
		throw vk::exception("Too big length of the useragent", __FILE__, __FUNCTION__, __LINE__);
	for (;i < length;i++)
		header.useragent[i] = p[i];	
	p += sizeof(decltype(header.useragent));

	header.statusCode = *reinterpret_cast<decltype(header.statusCode)*>(p);


	return header;
}


VK_NAMESPACE__VK_PROTO__END

