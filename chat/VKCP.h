#pragma once
#include <exception>
#include "../Container.h"

#define VKCP_CONNECT_TO_SERVER		"connect"
#define VKCP_DISCONNECT_FROM_SERVER	"disconnect"
#define VKCP_DISCONNECT_FROM_SERVER_STATUS_OK	"disconnect_ok"
#define VKCP_SENDING_MESSAGE		"sending_message"
#define VKCP_RECIEVING_MESSAGE		"recieving_message"

namespace vk::proto {


class VKCP {
public:


	typedef char				char_t;
	typedef char*				charp;
	typedef const char_t* const	ccharpc;
	typedef const char_t*		ccharp;
	typedef unsigned int		uint;
	typedef const uint			cuint;
	typedef const float			cfloat;


	static ccharpc			encoding;
	static cuint			size;
	static cuint			size_of_one_char;
	static cuint			username_max_len;
	static cuint			data_max_len;
	static cuint			encoding_max_len;
	static cfloat			version;
	static const char_t*	getExample()noexcept;
	static const char_t*	getExampleRules()noexcept;

	class VKCPHead {
	public:
		VKCPHead()noexcept;
		~VKCPHead();
		VKCPHead(const VKCPHead& obj)				noexcept;
		VKCPHead(VKCPHead&& obj)					noexcept;
		VKCPHead& operator=(const VKCPHead& obj)	noexcept;
		VKCPHead& operator=(VKCPHead&& obj)			noexcept;

		float		version;
		char_t*		encoding;
		uint		body_size;
		uint		self_id;
		uint		connect_id;
		char_t*		username;
		char_t*		data;
	};

	static bool				checkProto(ccharp src)noexcept(false);


	static VKCP::VKCPHead VKCP::parse(ccharp src)noexcept(false);
	static unsafe::Container<char_t> generate(
			cfloat version,
			ccharp encoding,
			cuint body_size,
			uint self_id,
			uint connect_id,
			ccharp username,
		ccharp data = nullptr
		) noexcept(false);

	static unsafe::Container<char_t> generate(
		cuint body_size,
		uint self_id,
		uint connect_id,
		ccharp username,
		ccharp data = nullptr
	) noexcept(false);
};
}



