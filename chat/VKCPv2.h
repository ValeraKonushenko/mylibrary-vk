#pragma once
#include "../namespaces_decl.h"
#include "../parser/ObjectParser.h"
#include "../Container.h"

VK_NAMESPACE__VK_PROTO__BEG

class VKCPv2 {
public:
	enum class MessageType : char {
		none,
		message,
		log_in_data,
		sign_up_data,
		other,
		answer
	};

	enum class StatusType : char {
		none,
		ok,				//200
		badResponse,	//400
		unuthorized,	//401
		notFound		//404
	};
	union Status {
		bool is_to_client;
		StatusType type;
	};
	typedef vk::unsafe::Container_c data_t;
	struct Header {
		float			version;
		MessageType		type;
		unsigned int	body_size;
		char			useragent[64];
		Status			statusCode;
	};
private:
	vk::unsafe::ObjectParser parser;
protected:
public:
	static const unsigned int size_of_header;
	static const float proto_version;
	static data_t generate	(float v, MessageType type, unsigned int bd_sz,
							const char* useragent, Status status)noexcept(false);
	static Header parse		(vk::unsafe::Container_c data)noexcept(false);
				VKCPv2			()						= delete;
				~VKCPv2			()						= delete;
				VKCPv2			(const VKCPv2& obj)		= delete;
				VKCPv2			(VKCPv2&& obj)			= delete;
	VKCPv2&		operator=		(const VKCPv2& obj)		= delete;
	VKCPv2&		operator=		(VKCPv2&& obj)			= delete;
};

VK_NAMESPACE__VK_PROTO__END