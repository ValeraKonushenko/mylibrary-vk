#include "SocketClient.h"
#include <locale>
#include <codecvt>
namespace vk::tcp::safe {
bool SocketClient::createSin(PCWSTR addr){
	SOCKADDR_IN sin = sc.sockaddr();
	sin.sin_family = AF_INET;
	sin.sin_port = htons(static_cast<u_short>(sc.port()));
	#ifdef _UNICODE
	auto res = InetPton(AF_INET, addr, &sin.sin_addr);
	#else
	int len = wcslen(addr);
	char* p = new char[len + 1]{};
	const wchar_t* fp = addr;
	for (int i = 0; i < len; i++) {
		p[i] = static_cast<CHAR>(*fp);
		fp++;
	}
	auto res = InetPton(AF_INET, p, &sin.sin_addr);
	delete[]p;
	#endif
	sc.sockaddr(sin);
	return true;
}
bool SocketClient::open(port p, PCWSTR addr) noexcept {
	std::lock_guard lg(recmut);
	if (isOpen())close();
	if (!createSocket(p))return false;
	if (!createSin(addr))return false;
	printStep((
		std::string("Client's port: ") + 
		std::to_string(p) + 
		std::string(" was successful opened to address: ") +
		this->getInetNtop()
		).c_str());
	return true;
}
void SocketClient::send(const Buffer& bf, size_t size, int flags) const noexcept{
	std::lock_guard lg(recmut);
	Socket::send(sc, bf, size, flags);
}
bool SocketClient::connect() noexcept{
	SOCKADDR_IN sin = sc.sockaddr();

	if (::connect(
		this->sc.socket(),
		reinterpret_cast<SOCKADDR*>(&sin),
		sizeof(sin)) == -1
		) {
		vk::safe::Logger::push(Log(
			"Error with connecting to: " + 
			getInetNtop() + ":" + std::to_string(sc.port())
		));
		close();
		return false;
	}
	vk::safe::Logger::push(Log(
		"Success connecting to: " +
		getInetNtop() + ":" + std::to_string(sc.port())
	));
	return true;
}
std::string SocketClient::getInetNtop()	const noexcept{
	std::lock_guard lg(recmut);
	size_t i = 0u;
	wchar_t*buff = new wchar_t[INET6_ADDRSTRLEN];
	char*	out = new char[INET6_ADDRSTRLEN];
	SOCKADDR_IN sin = sc.sockaddr();
	#ifdef _UNICODE
	InetNtop(AF_INET, &sin.sin_addr, buff, INET6_ADDRSTRLEN);
	#else
	int len = INET6_ADDRSTRLEN;
	char* p = new char[len + 1]{};
	const wchar_t* fp = buff;
	for (int i = 0; i < len; i++) {
		p[i] = static_cast<CHAR>(*fp);
		fp++;
	}
	InetNtop(AF_INET, &sin.sin_addr, p, INET6_ADDRSTRLEN);
	delete[]p;
	#endif
	for (i = 0; i < INET6_ADDRSTRLEN - 1 && buff[i]; i++) {
		out[i] = static_cast<char>(buff[i]);
	}
	out[i] = 0;
	std::string sout(out);
	delete[]buff;
	delete[]out;
	return sout;
}
SocketClient::SocketClient(const SocketClient& obj){
	*this = obj;
}
SocketClient& SocketClient::operator=(const SocketClient&obj){
	std::scoped_lock sl(recmut, obj.recmut);
	this->sc = obj.sc;
	return *this;
}
SocketClient::SocketClient(SocketClient&&obj)noexcept{
	*this = std::move(obj);
}
SocketClient& SocketClient::operator=(SocketClient&&obj)noexcept {
	std::scoped_lock sl(recmut, obj.recmut);
	this->sc = std::move(obj.sc);
	return *this;
}
}


namespace vk::tcp::unsafe {
bool SocketClient::createSin(PCWSTR addr) {
	SOCKADDR_IN sin = sc.sockaddr();

	sin.sin_family = AF_INET;
	sin.sin_port = htons(static_cast<u_short>(sc.port()));
	#ifdef _UNICODE
	auto res = InetPton(AF_INET, addr, &sin.sin_addr);
	#else
	int len = wcslen(addr);
	char* p = new char[len + 1]{};
	const wchar_t* fp = addr;
	for (int i = 0; i < len; i++) {
		p[i] = static_cast<CHAR>(*fp);
		fp++;
	}
	auto res = InetPton(AF_INET, p, &sin.sin_addr);
	delete[]p;
	#endif

	sc.sockaddr(sin);
	return true;
}
bool SocketClient::open(port p, PCWSTR addr) noexcept {
	if (isOpen())close();
	if (!createSocket(p))return false;
	if (!createSin(addr))return false;
	printStep((
		std::string("Client's port: ") +
		std::to_string(p) +
		std::string(" was successful opened to address: ") +
		this->getInetNtop()
		).c_str());
	return true;
}
void SocketClient::send(const Buffer& bf, size_t size, int flags) const noexcept {
	Socket::send(sc, bf, size, flags);
}
bool SocketClient::connect() noexcept {
	SOCKADDR_IN sin = sc.sockaddr();

	if (::connect(
		this->sc.socket(),
		reinterpret_cast<SOCKADDR*>(&sin),
		sizeof(sin)) == -1
		) {
		vk::safe::Logger::push(Log(
			"Error with connecting to: " +
			getInetNtop() + ":" + std::to_string(sc.port())
		));
		close();
		return false;
	}
	vk::safe::Logger::push(Log(
		"Success connecting to: " +
		getInetNtop() + ":" + std::to_string(sc.port())
	));
	return true;
}
std::string SocketClient::getInetNtop()	const noexcept {
	size_t i = 0u;
	wchar_t* buff = new wchar_t[INET6_ADDRSTRLEN];
	char* out = new char[INET6_ADDRSTRLEN];
	SOCKADDR_IN sin = sc.sockaddr();
	#ifdef _UNICODE
	InetNtop(AF_INET, &sin.sin_addr, buff, INET6_ADDRSTRLEN);
	#else
	int len = INET6_ADDRSTRLEN;
	char* p = new char[len + 1]{};
	const wchar_t* fp = buff;
	for (int i = 0; i < len; i++) {
		p[i] = static_cast<CHAR>(*fp);
		fp++;
	}
	InetNtop(AF_INET, &sin.sin_addr, p, INET6_ADDRSTRLEN);
	delete[]p;
	#endif
	for (i = 0; i < INET6_ADDRSTRLEN - 1 && buff[i]; i++) {
		out[i] = static_cast<char>(buff[i]);
	}
	out[i] = 0;
	std::string sout(out);
	delete[]buff;
	delete[]out;
	return sout;
}
SocketClient::SocketClient(const SocketClient& obj) {
	*this = obj;
}
SocketClient& SocketClient::operator=(const SocketClient& obj) {
	this->sc = obj.sc;
	return *this;
}
SocketClient::SocketClient(SocketClient&& obj) noexcept {
	*this = std::move(obj);
}
SocketClient& SocketClient::operator=(SocketClient&& obj) noexcept {
	this->sc = std::move(obj.sc);
	return *this;
}
}