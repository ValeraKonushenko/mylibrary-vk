#include "Assert.h"

std::runtime_error vk::exception(std::string_view message, std::string_view file, std::string_view func, long long line){
	using namespace std;
	string error;
	error += "Error at ";
	error += func;
	error += ":";
	error += to_string(line);
	error += " line\nIn the file: ";
	error += file;
	error += " \n\nDetail: ";
	error += message;

	return std::runtime_error(error);
}
