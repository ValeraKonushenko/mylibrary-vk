#include "Pack.h"
VK_NAMESPACE__VK_UDP_SAFE__BEG

void Pack::dataToPack(size_t size, const char* data) noexcept(false){
    std::lock_guard lg(recmut);
    const char* p = data;
    Package part;

    while (p <= data + size) {
        part.clear();
        part.setBufferSize(def_pack_size);
        part.set(def_pack_size, p);
        pack.push_back(part);
        p += def_pack_size;
    }
}

void Pack::setBuffSize(size_t size) noexcept{
    std::lock_guard lg(recmut);
    def_pack_size = size;
}

size_t Pack::getBuffSize() const noexcept{
    std::lock_guard lg(recmut);
    return def_pack_size;
}

size_t Pack::getAmoOfPackages() const noexcept{
    std::lock_guard lg(recmut);
    return amo_of_packages;
}

bool Pack::isEmpty() const noexcept{
    std::lock_guard lg(recmut);
    return pack.empty();
}

size_t Pack::size() const noexcept{
    std::lock_guard lg(recmut);
    return pack.size();
}

void Pack::clear() noexcept{
    std::lock_guard lg(recmut);
    pack.clear();
}

Package Pack::get(size_t i) const noexcept(false){
    std::lock_guard lg(recmut);
    if (i >= pack.size())
        throw std::runtime_error("Too big value for index of packages.");
    return pack[i];
}

const Package& Pack::getCRef(size_t i) const noexcept(false){
    std::lock_guard lg(recmut);
    if (i >= pack.size())
        throw std::runtime_error("Too big value for index of packages.");
    return pack[i];
}

void Pack::set(size_t i, const Package& el) noexcept(false){
    std::lock_guard lg(recmut);
    if (i >= pack.size())
        throw std::runtime_error("Too big value for index of packages.");
    pack[i] = el;
}

void Pack::set(size_t i, const char* data) noexcept(false){
    std::lock_guard lg(recmut);
    clear();
    amo_of_packages = ceil(1.*i / def_pack_size);
    dataToPack(i, data);
}

Pack::Pack() noexcept{
    clear();
    def_pack_size = 0u;
}

Pack::~Pack(){
    clear();
}

Pack::Pack(const Pack& obj) noexcept(false){
    std::scoped_lock lg(recmut, obj.recmut);
    *this = obj;
}

Pack::Pack(Pack&& obj) noexcept(false){
    std::scoped_lock lg(recmut, obj.recmut);
    *this = std::move(obj);
}

Pack& Pack::operator=(const Pack& obj) noexcept(false){
    pack = obj.pack;
    def_pack_size = obj.def_pack_size;
    return *this;
}

Pack& Pack::operator=(Pack&& obj) noexcept(false){
    pack = std::move(obj.pack);
    def_pack_size = obj.def_pack_size;
    obj.clear();
    return *this;
}

VK_NAMESPACE__VK_UDP_SAFE__END