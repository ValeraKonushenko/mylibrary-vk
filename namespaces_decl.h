#pragma once
#define VK_NAMESPACE__VK__BEG namespace vk{
#define VK_NAMESPACE__VK__END }

#define VK_NAMESPACE__VK_PROTO__BEG namespace vk{ namespace proto{
#define VK_NAMESPACE__VK_PROTO__END }}

#define VK_NAMESPACE__VK_SAFE__BEG namespace vk{ namespace safe{
#define VK_NAMESPACE__VK_SAFE__END }}

#define VK_NAMESPACE__VK_UNSAFE__BEG namespace vk{ namespace unsafe{
#define VK_NAMESPACE__VK_UNSAFE__END }}

#define VK_NAMESPACE__VK_TCP_SAFE__BEG namespace vk{ namespace tcp{ namespace safe{
#define VK_NAMESPACE__VK_TCP_SAFE__END }}}

#define VK_NAMESPACE__VK_TCP_UNSAFE__BEG namespace vk{ namespace tcp{ namespace unsafe{
#define VK_NAMESPACE__VK_TCP_UNSAFE__END }}}

#define VK_NAMESPACE__VK_UDP_SAFE__BEG namespace vk{ namespace UDP{ namespace safe{
#define VK_NAMESPACE__VK_UDP_SAFE__END }}}
