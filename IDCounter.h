#pragma once
#include "namespaces_decl.h"
#include <mutex>
VK_NAMESPACE__VK_SAFE__BEG
class IDCounter {
public:
	typedef unsigned long long	id_t;
private:
protected:
	std::recursive_mutex recmut;
	static id_t id_counter;
public:
	id_t getNewID()noexcept(false);
	IDCounter()								= default;
	~IDCounter()							= default;
	IDCounter(const IDCounter&)				= delete;
	IDCounter(IDCounter&)					= delete;
	IDCounter& operator=(const IDCounter&)	= delete;
	IDCounter& operator=(IDCounter&)		= delete;
};
VK_NAMESPACE__VK_SAFE__END