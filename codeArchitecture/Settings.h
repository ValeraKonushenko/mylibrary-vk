#pragma once
#include "../namespaces_decl.h"
#include "../parser/ObjectParser.h"
#include <string>
#include <map>
#include <mutex>
VK_NAMESPACE__VK_SAFE__BEG

class SettingsData : public std::string {
public:
	SettingsData();
	SettingsData(const char* str);
	int			toInt		()		const noexcept(false);
	double		toDouble	()		const noexcept(false);
	const char* toCCharp	()		const noexcept(false);
	long long	toLongLong	()		const noexcept(false);
	operator int			()		const noexcept(false);
	operator double			()		const noexcept(false);
	operator long long		()		const noexcept(false);
};

class Settings {
public:
	using ParserStatus = vk::unsafe::ObjectParser::Status;
	typedef vk::unsafe::ObjectParser::value_t property;
	typedef SettingsData			string;
	typedef std::string_view		string_view;
private:
	string path_to_settings;
	vk::unsafe::ObjectParser parser;
protected:
	mutable std::recursive_mutex rec_mut;
public:
	bool				isEmpty			()							const noexcept;
	bool				isProperty		(string_view pr)			const noexcept;
	string				getProperty		(string_view pr)			const noexcept(false);
	string				getProperty		(size_t i)					const noexcept(false);
	string				operator[]		(string_view pr)			const noexcept(false);
	string				operator[]		(size_t i)					const noexcept(false);
	size_t				countOfProperty	()							const noexcept(false);
	Settings&			setProperty		(string_view pr, string value)noexcept(false);
	virtual Settings&	loadSettings	()							noexcept(false);
	virtual Settings&	saveSettings	()							noexcept(false);
	string				getPath			()							const noexcept;
	Settings&			setPath			(const char* path)			noexcept(false);
	Settings&			clear			()							noexcept;
						Settings		()							noexcept;
	virtual				~Settings		();
						Settings		(const Settings&obj)		= delete;
						Settings		(Settings&&obj)				= delete;
	Settings&			operator=		(const Settings& obj)		= delete;
	Settings&			operator=		(Settings&& obj)			= delete;
};

VK_NAMESPACE__VK_SAFE__END